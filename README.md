![alt tag](images/repo-logo.jpg)

# Paintings by Milchman - Wordpress Custom Theme #

* Version: 1.0.0
* Design: SMG

### Componentes Principales ###

* Twitter Bootstrap 4.5.3

### Funciones Incluídas ###

* Custom Post Type.
* Custom Taxonomies.
* Bootstrap Ready: Wordpress Menu Structure.
* Custom Metabox.

### Plugins Requeridos ###

* CMB2
* Jetpack by Wordpress

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: SMG
Main Developer: [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=paintings)
