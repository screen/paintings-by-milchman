<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

/*
function paintings_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'paintings' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'paintings' ),
		'menu_name'             => __( 'Clientes', 'paintings' ),
		'name_admin_bar'        => __( 'Clientes', 'paintings' ),
		'archives'              => __( 'Archivo de Clientes', 'paintings' ),
		'attributes'            => __( 'Atributos de Cliente', 'paintings' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'paintings' ),
		'all_items'             => __( 'Todos los Clientes', 'paintings' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'paintings' ),
		'add_new'               => __( 'Agregar Nuevo', 'paintings' ),
		'new_item'              => __( 'Nuevo Cliente', 'paintings' ),
		'edit_item'             => __( 'Editar Cliente', 'paintings' ),
		'update_item'           => __( 'Actualizar Cliente', 'paintings' ),
		'view_item'             => __( 'Ver Cliente', 'paintings' ),
		'view_items'            => __( 'Ver Clientes', 'paintings' ),
		'search_items'          => __( 'Buscar Cliente', 'paintings' ),
		'not_found'             => __( 'No hay resultados', 'paintings' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'paintings' ),
		'featured_image'        => __( 'Imagen del Cliente', 'paintings' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'paintings' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'paintings' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'paintings' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'paintings' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'paintings' ),
		'items_list'            => __( 'Listado de Clientes', 'paintings' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'paintings' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'paintings' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'paintings' ),
		'description'           => __( 'Portafolio de Clientes', 'paintings' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'paintings_custom_post_type', 0 );
*/
